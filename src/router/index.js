import Vue from 'vue'
import Router from 'vue-router'
import ExperiencesSection from '@/components/ExperiencesSection'
import HomeSection from '@/components/HomeSection'
import WhoSection from '@/components/WhoSection'
import ProjetsSection from '@/components/ProjetsSection'
import ContactSection from '@/components/ContactSection'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'Home',
      component: HomeSection
    },
    {
      path: '/who',
      name: 'Who',
      component: WhoSection
    },
    {
      path: '/projets',
      name: 'Projets',
      component: ProjetsSection
    },
    {
      path: '/experiences',
      name: 'Experiences',
      component: ExperiencesSection
    },
    {
      path: '/contact',
      name: 'Contact',
      component: ContactSection
    }
  ]
})
