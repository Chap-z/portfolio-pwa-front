var $ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');
require('popper.js');



// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios';


axios.defaults.baseURL = 'https://charlottel.promo-17.codeur.online/portfolio-pwa/api';
Vue.config.productionTip = false

/* eslint-disable no-new */
export const globalData = new Vue({

  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'

});

if ('serviceWorker' in navigator) {
  console.log('service worker supporté');
  navigator.serviceWorker
    .register('service-worker.js', {
      scope: './'
    })
    .then(function (registration, ) {
      console.log("Service Worker Registered", registration);
    })
    .catch(function (err) {
      console.log("Service Worker Failed to Register", err);
    })

}
