import axios from 'axios'

export default {
  getItems() {
    return axios.get('/items')
      .then(response => {
        return response.data

      })
  },

  // getItem(id) {
  //   return axios.get('/items/' + id)
  //     .then(response => {
  //       return response.data
  //     })

  // }
}
