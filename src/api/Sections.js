import axios from 'axios'


export default {
  getSections() {
    return axios.get('/sections')
      .then(response => {
        return response.data
      })
  },

  getSection(id) {
    return axios.get('/sections/' + id).then(response => {
      return response.data
    })

  }
}
